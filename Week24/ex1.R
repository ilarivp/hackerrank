data<-matrix(c("7 11","10000 15","3 2", "0 0 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6 1 6 6 6 6 6 6 6 6 6 6", "5 -6"), ncol=1)

datalist <- strsplit(data, " ")
house <- as.numeric(datalist[[1]])
s <- house[1]
t <- house[2]

trees <- as.numeric(datalist[[2]])
a <- trees[1]
b <- trees[2]


numfruit <- as.numeric(datalist[[3]])
m <- numfruit[1]
n <- numfruit[2]

appled <- as.numeric(datalist[[4]])
oranged <- as.numeric(datalist[[5]])
drop.apple <- a + as.numeric(appled)
drop.orange <- b + as.numeric(oranged)

hit.apple <- as.numeric(length(drop.apple[drop.apple>=s & drop.apple<=t]))
hit.orange <- as.numeric(length(drop.orange[drop.orange>=s & drop.orange<=t]))

dataout <- matrix(c(hit.apple, hit.orange), ncol = 1)

cat(dataout, sep="\n") 
