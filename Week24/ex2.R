#first line of input is the number of games
#after that 1. number of cells on board, 2. string describing ladybugs (RBC_CRB)

nums<-matrix(c("4","7","RBY_YBR", "6", "X_Y__X", "2", "__", "6", "B_RRBR"), ncol=1)
num.games <- as.numeric(nums[1])
def.games <- nums[-1,]

answer.matrix = matrix(c("YES"), nrow=num.games, ncol=1)
selector.vector = c(F,T)
ladybug.string = def.games[selector.vector]


resultchar <- matrix(c(0), ncol=length(ladybug.string))
for (j in 1:length(ladybug.string)) {
  
  unique.string <- unique(strsplit(ladybug.string[j] ,"")[[1]])
  if (unique.string[1]!="_"){
  unique.string <- unique.string[grepl('[A-Za-z]', unique.string)]
  minchar<-matrix(c(10), nrow=length(unique.string))
  for (i in 1:length(unique.string)){
    minchar[i,1]<-lengths(regmatches(ladybug.string[j], gregexpr(unique.string[i], ladybug.string[j])))
  }

  resultchar[1, j] <- min(minchar)
  } else {
  resultchar[1,j] <- 2
  }
}



answer.vector <- (!grepl('[A-Za-z]', ladybug.string) | resultchar!=1)

answer.matrix[answer.vector==F]<- "NO"
