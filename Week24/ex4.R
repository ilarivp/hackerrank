#ex4
input.Matrix <- matrix(c("9 10", "1 2 3 4 5 6 7 8 9"), ncol=1)

m.Dim <- as.double(strsplit(input.Matrix[1], " ")[[1]])
m.input <- as.double(strsplit(input.Matrix[2], " ")[[1]])

#first row
conv.Bits <- m.input

#XOR function
#a_ij=a_i-1j {XOR} a_i-1j+1, 0<=j<=n-2
ans.Matrix <- list(m.input)
next.Bits <- matrix(c(0), ncol=m.Dim[1])
if (m.Dim[2]>=2) {
loop.Rows <- as.double(c(2:m.Dim[2]))
for (j in 2:m.Dim[2]) {
  for (i in 1:(m.Dim[1]-1)) {
    next.Bits[i] <- bitwXor(conv.Bits[i], conv.Bits[i+1])
  }
  next.Bits[m.Dim[1]] <- bitwXor(conv.Bits[m.Dim[1]], conv.Bits[1])
  conv.Bits <- next.Bits
  ans.Matrix <- c(ans.Matrix, list(as.vector(conv.Bits))) 
}
  
}
# test.Matrix <- conv.Bits
# cat(test.Matrix)
ans.Matrix
cat(ans.Matrix[[length(ans.Matrix)]])

# pow.2 <- function(x) {
#   if (x!=0 && bitwAnd(x, (x-1))==0){
#   return(T)
#   } else {
#     return(F)
#   }
# } 
# 
# pow.2(10)

#2 potenssit antaa lopulta nollarivin
#