import requests
from bs4 import BeautifulSoup
import sys

def twinArrays(ar1, ar2):
    # Complete this function

n = int(input().strip())
ar1 = list(map(int, input().strip().split(' ')))
ar2 = list(map(int, input().strip().split(' ')))
result = twinArrays(ar1, ar2)
print(result)