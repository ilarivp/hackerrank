# Enter your code here. Read input from STDIN. Print output to STDOUT
#f <- file("stdin")
#on.exit(close(f))
#nums <- readLines(f)
rm(list=ls())
nums<-matrix(c("31"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
n.val <- as.numeric(datalist[[1]][1])
m.val <- as.numeric(datalist[[1]][2])
#Pseudo


#jos parillinen ja 2 et�isyys => 0
#jos et�isyys 1 => 0
if ((m.val-n.val==2&n.val%%2==0)|(m.val-n.val<2)){
  ans<-0
} else {
  if (n.val%%2==0) {
    n.val<-n.val+1
  }
  
    #List odd numbers from range
    val.range <- seq(from=n.val, to=m.val, by=2)
    val.range1 <- val.range[sapply(val.range, function(x) sum(x/1:x==x%/%1:x))==2]
    val.range2 <- val.range1+2
    
    for (i in 1:(length(val.range1)-1)){
      if(val.range2[i]!=val.range1[i+1]) {
        val.range1[i]<-0
      }
    }
    
    ans.range <- val.range1[val.range1!=0]
    if (length(ans.range)==0){
      ans <- 0
    } else {
      ans <- length(ans.range)-1
    }
}

cat(ans)