#w26_ex5
#given positive integer n, find and print the number of pairs of (a,b), where a<b, x*a+y*b=n
#a,b,y,x \in positive integers
# 4<= n <= 3*10^5
rm(list=ls())
nums <- matrix(c("10"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
n <- as.numeric(datalist[[1]][1])
#scout possible a and b (must be divisors of n)

#b is max n-1, min 2
#a is max b-1, min 1

brange <- c(n-1, 2)
arange <- c(n-2, 1)

#start at max range of b and min range a
b <- brange[2]
a <- 1
counter <- 0
while (b<=brange[1]) {
while (a<=b-1) {
  #select only y values that produce integer x values that solve equation
  y <- 1:floor(n/a)
  y <- y[((n-y*b)/a)>0&((n-y*b)/a)%%1==0]
  #calculate possible x values
  x <- (n-y*b)/a
  
  #testvalue
  checkval <- a*x+b*y
  
  #create vector to count suitable x values
  countvec <- rep(F, length(x))
  #turn all suitable x values to "TRUE"
  countvec[x%%1==0&x>0] <- T
  #if even one true, then add to counter (also for testing, print values of a,x,b,y and satisfactory equation)
    if(sum(countvec)>=1) {
      counter <- counter+1
      print(paste("START a:", a, "b:", b, sep=" "))
      print(paste("x:", x, sep=" "))
      print(paste("y:", y, sep=" "))
      print(paste(n, "=", checkval))
}
  a <- a+1
}
a <- 1
b <- b+1
}
ans <- counter
cat(ans)
