#w26_ex1
rm(list=ls())
nums<-matrix(c("2 3"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
n.val <- as.numeric(datalist[[1]][1])
m.val <- as.numeric(datalist[[1]][2])


n.dr <- floor(n.val/2)+n.val%%2
m.dr <- floor(m.val/2)+m.val%%2

ans <- n.dr*m.dr

cat(ans)