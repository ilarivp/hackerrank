# Enter your code here. Read input from STDIN. Print output to STDOUT
#f <- file("stdin")
#on.exit(close(f))
#nums <- readLines(f)
rm(list=ls())
nums<-matrix(c("999000000 1000000000"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
n.val <- as.numeric(datalist[[1]][1])
m.val <- as.numeric(datalist[[1]][2])
#Pseudo


sieve <- function(num){
  values <- rep(TRUE, num)
  values[1] <- FALSE
  prev.prime <- 2
  for(i in prev.prime:sqrt(num)){
    values[seq.int(2 * prev.prime, num, prev.prime)] <- FALSE
    prev.prime <- prev.prime + min(which(values[(prev.prime + 1) : num]))
  }
  return(which(values))
}
#jos parillinen ja 2 et�isyys => 0
#jos et�isyys 1 => 0
if ((m.val-n.val==2&n.val%%2==0)|(m.val-n.val<2)){
  ans<-0
} else if (n.val==1&(m.val==3|m.val==4)) {
  ans<-1
} else {
  
  if (n.val%%2==0) {
    n.val<-n.val+1
  }
  
  
  val.range <- sieve(100000)
  
  
  
  
  val.range <- val.range[val.range!=0]
  if (length(val.range)==0){
    ans <- 0
  } else {
    ans <- length(val.range)-1
  }
  
}

cat(ans)