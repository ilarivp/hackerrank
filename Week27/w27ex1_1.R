#w27ex1_1
# Enter your code here. Read input from STDIN. Print output to STDOUT
#f <- file("stdin")
#on.exit(close(f))
#nums <- readLines(f)
rm(list=ls())
nums<-matrix(c("8", "5"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
n <- as.numeric(datalist[[1]][1])
p <- as.numeric(datalist[[2]][1])

if((n-p)<p) {
  ans <- floor((n-p)/2)
} else {
  ans <- floor(p/2)
}
cat(ans)