#Hackerrank codeweek25 ex1
nums<-matrix(c("2 3","5 6","16 32 96"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
A.Len <- as.numeric(datalist[[1]][1])
B.Len <- as.numeric(datalist[[1]][2])
A.Ser <- as.numeric(datalist[[2]])
B.Ser <- as.numeric(datalist[[3]])

gcd <- function(x,y) {
  r <- x%%y;
  return(ifelse(r, gcd(y, r), y))
}

#Find first common factor
i = matrix(c(1:100), nrow=A.Len, ncol=100, byrow=T)
gcd.Sel <- i%%A.Ser==0
gcd.jump <- 100
for (j in 1:100) {
  if(all(gcd.Sel[,j]==T)){
    gcd.jump <- as.numeric(i[1,j])
    break
  }
}

counter=0
if (gcd.jump<B.Ser[1]) {
  temp.Seq <- seq(from=gcd.jump, to=B.Ser[1], by=gcd.jump)
  i = matrix(temp.Seq, nrow=B.Len, length(temp.Seq), byrow=T)
  gcd.Sel2 <- B.Ser%%i==0
  for (j in 1:length(temp.Seq)){
    if(all(gcd.Sel2[,j]==T)){
      counter=counter+1
    }
  }
} else {
counter=0
  
}
cat(counter)

