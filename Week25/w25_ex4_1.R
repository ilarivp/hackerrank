#Hackerrank codeweek25 ex3
rm(list=ls())
nums<-matrix(c("15 3","5 2 3"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
#number of initial stones
init.n <- as.numeric(datalist[[1]][1])
init.m <- as.numeric(datalist[[1]][2])
#different sets
init.s <- as.numeric(datalist[[2]])


prime = function(n) {
  n = as.integer(n)
  if(n > 1e8) stop("n too large")
  primes = rep(TRUE, n)
  primes[1] = FALSE
  last.prime = 2L
  fsqr = floor(sqrt(n))
  while (last.prime <= fsqr)
  {
    primes[seq.int(2L*last.prime, n, last.prime)] = FALSE
    sel = which(primes[(last.prime+1):(fsqr+1)])
    if(any(sel)){
      last.prime = last.prime + min(sel)
    }else last.prime = fsqr+1
  }
  which(primes)
}


#loop through different sets
divs.matrix <- matrix(c("None"), nrow=init.m)
for (i in 1:init.m) {
  #determine amount of divisions to 1
  div.res <- init.n/init.s[i]
  div.count <- 0
  while (div.res%%floor(div.res)==0 & div.res>1) {
    div.res <- div.res/prime(div.res)[1]
    div.count<- div.count+1
  }
  num.divs <- init.s[i]*div.count
  if (num.divs==0) {
    divs.matrix[i] <- "None"
  } else if (num.divs%%2==0){
    divs.matrix[i] <- "First"
  } else {
    divs.matrix[i] <- "Second"
  }
}
ans <- divs.matrix[divs.matrix[,1]!="None"][1]
cat(ans)