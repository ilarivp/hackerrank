#!/bin/python3

import sys

def onceInATram(x):
    # Complete this function
    x = x+1
    x1num = [int(n) for n in str(x)[0:3]]
    x2num = [int(n) for n in str(x)[3:6]]
    while sum(x1num) != sum(x2num) :
        x = x+1
        x1num = [int(n) for n in str(x)[0:3]]
        x2num = [int(n) for n in str(x)[3:6]]
    ans = str(x)
    return(ans)

if __name__ == "__main__":
    x = int(input().strip())
    result = onceInATram(x)
    print(result)
