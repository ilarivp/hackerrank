# f <- file("stdin")
# on.exit(close(f))
# nums <- readLines(f)

# nums <- c('3', '1 2 3', '1 2 3')
# nums <- strsplit(nums, ' ', fixed=T)
# n <- as.numeric(nums[[1]])
# a <- as.numeric(nums[[2]])
# b <- as.numeric(nums[[3]])

# Script for finding GCD and sum based on calculating largest multiple of primes
# Calculates list of primes up to n
# Loop through primes to find largest multiple in A
# Loop through primes to find largest multiple in B
# Loop through factor vector to find value in B that has same common denominator
library(data.table)

test <- 5e5
a <- data.table(a = as.integer(round(runif(n = test, min = 1, max = 1e6), 0)))
b <- data.table(b = as.integer(round(runif(n = test, min = 1, max = 1e6), 0)))
a <- a[order(-a)]
b <- b[order(-b)]





findfactorials <- function(n) {
  seq_max <- as.integer(min(n, 50L))
  temp <- data.table(temp = floor(n/seq_len(seq_max)))
  temp <- temp[(n %% temp == 0L)]
  div <- seq_len(as.integer(n/seq_max))
  factors <- c(1, div[n %% div == 0L], temp)
  return(temp)
}

findfactorials1 <- function(n) {
  seq_max <- as.integer(min(n, 50L))
  temp <- data.table(temp = floor(n/seq_len(seq_max)))
  temp <- temp[(n %% temp == 0L)]
  div <- seq_len(as.integer(n/seq_max))
  factors <- c(1, div[n %% div == 0L], temp)
  return(temp)
}

test.time<- system.time(for (i in 1:50000) findfactorials(1e6))  # Fastest
test.time1<- system.time(for (i in 1:500000) findfactorials1(1e6))


allPrime <- function(n) {
  primes <- rep(TRUE, n)
  primes[1] <- FALSE
  for (i in 1:sqrt(n)) {
    if (primes[i]) primes[seq(i^2, n, i)] <- FALSE
  }
  which(primes)
}




largestCommonFactor <- function(n, a, b) {
  if (sum(a%%n==0L)>0&sum(b%%n==0L)>0) {
    nseq <- seq.int(from=n, to=1e6, by=n)
    for (i in nseq){
    maxnum <- which.min(nseq[i]==a&nseq[i]==b)
    }
  }
}

is.prime <- function(n) n == 2L || all(n %% 2L:max(2,floor(sqrt(n))) != 0)


test <- system.time(allPrime(1e6))


getFactors <- function(a){
  #  list all factors for vectors a
  #  1. remove primes from list
  #  2. find factorials for vector a
  #  3. unlist and format
  factors <- as.integer(a)
  primebool <- sapply(factors, function(x) is.prime(x))  # remove primes
  if (sum(primebool)>0) {  # If primes found, remove them from factor check
    factors <- factors[!(primebool)]
  }
  if (length(factors)>0) {
    factors <- sapply(factors, function(x) findfactorials(x))
    factors <- unlist(factors, recursive = F)
  } 
  if (sum(primebool)>0) factors <- as.integer(c(factors, as.integer(a[primebool])))
  factors <- factors[order(-factors)]
  factors <- factors[duplicated(factors)==F]
  factors <- c(factors, 1L)
}

# Select shorter of two lists
if (max(a)>=max(b)) {
  test.time <- system.time(facts <- getFactors(b))
} else {
  test.time <- system.time(facts <- getFactors(a))
}
test.time


i=1
sum.a<-0
sum.b<-0
while ((sum.a < 1 | sum.b < 1)) {
  facts.a <- (a%%facts[i]==0)
  facts.b <- b%%facts[i]==0
  sum.a <- sum(facts.a)
  sum.b <- sum(facts.b)
  i=i+1
}
facts.a <- a%%facts[i-1]==0
facts.b <- b%%facts[i-1]==0
max.a <- a[min(which(facts.a))]
max.b <- b[min(which(facts.b))]
ans <- max.a+max.b

cat(ans)

