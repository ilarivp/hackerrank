#!/bin/python3

import sys

def maximumGcdAndSum(A, B):
    # Complete this function
    max_val = max(max(A), max(B))
    def get(A, max_value):
        okay =  [0 for i in range(max_value+1)]
        for x in A:
            okay[x] = x
        for x in range(1, max_value+1):
            for y in range(x+x, max_value +1, x):
                if okay[y]:
                    okay[x] = y
        return okay
    okayA = get(A, max_val)
    okayB = get(B, max_val)
    for i in range(max_val, 0, - 1):
        if okayA[i] > 0 and okayB[i] > 0:
            return(okayA[i] + okayB[i])

if __name__ == "__main__":
    n = int(input().strip())
    A = list(map(int, input().strip().split(' ')))
    B = list(map(int, input().strip().split(' ')))
    res = maximumGcdAndSum(A, B)
    print(res)
