#week30ex2
# Enter your code here. Read input from STDIN. Print output to STDOUT
#f <- file("stdin")
#on.exit(close(f))
#nums <- readLines(f)
rm(list=ls())
nums<-matrix(c("1"), ncol=1)
datalist <- strsplit(nums, " ", fixed=TRUE)
#max length
n <- as.numeric(datalist[[1]][1])

vowel <- tolower(c('A', 'E', 'I', 'O', 'U'))
consonant <- tolower(c('B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Z'))

alphabet <- matrix(vowel, ncol=1)
alphabet <- cbind(alphabet, rep('v', length(vowel)))
alphabet1 <- matrix(consonant, ncol=1)
alphabet1 <- cbind(alphabet1, rep('c', length(consonant)))
alphabet <- rbind(alphabet, alphabet1)
rm(alphabet1)
lm <- matrix(rep(1,n),ncol=n)
pwm <- matrix(rep(1,n), ncol=n)
ans <- NULL
recursion <- function(x, c) {
  if(c==0){return(ans)} else {
  if(substr(x[1], n-c,1)== 'A') {
    for (i in 1:length(consonant)) {
      return(paste0(recursion(ans, c-1),consonant[i], vowel, '\n'))
      }
    } else {
      for (j in 1:length(vowel)) {
      return(paste0(recursion(ans, c-1), vowel[j], consonant, '\n'))
      }
    }
  }
}
test <- recursion(consonant, 2)
cat(paste0(vowel[i], consonant, '\n'))
rectest <- function(x,n) {
  if (n==0) {return(ans1) }
  else {
    #ans1 <- paste0(ans1, 'a','\n')
    return(paste0(ans1,'\n', rectest(ans1, n-1)))
  }
}
ans1 <- c('a')
rectest(ans1,10)
ans <- paste0(alphabet[,1], sep='\n')

cat(ans)
F <- function(n) ifelse(n == 0, 1, n - M(F(n-1)))
M <- function(n) ifelse(n == 0, 0, n - F(M(n-1)))

print.table(lapply(0:19, M))
print.table(lapply(0:19, F))
